package com.sader_dev.vosk_voice_to_text_basic_implementation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sader_dev.vosk_voice_to_text_basic_implementation.Option
import com.sader_dev.vosk_voice_to_text_basic_implementation.R

class listAdapter (private val list: List<String>): RecyclerView.Adapter<listViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): listViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return listViewHolder(layoutInflater.inflate(R.layout.item_list, parent, false))

    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: listViewHolder, position: Int) {
        val item = list[position]
        holder.render(item)



    }


}