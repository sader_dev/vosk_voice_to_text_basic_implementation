package com.sader_dev.vosk_voice_to_text_basic_implementation

import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import org.json.JSONObject
import org.vosk.LibVosk
import org.vosk.LogLevel
import org.vosk.Model
import org.vosk.Recognizer
import org.vosk.android.RecognitionListener
import org.vosk.android.SpeechService
import org.vosk.android.StorageService
import java.io.IOException
import kotlin.Exception





class MainActivity : AppCompatActivity() , RecognitionListener {

    private lateinit var options: List<Option>
    private lateinit var optionA: Option
    private lateinit var optionQ: Option
    private lateinit var optionK: Option
    private lateinit var optionJ: Option
    private lateinit var optionT: Option


    private lateinit var option0: Option
    private lateinit var option1: Option
    private lateinit var option2: Option
    private lateinit var option3: Option
    private lateinit var option4: Option
    private lateinit var option5: Option
    private lateinit var option6: Option
    private lateinit var option7: Option
    private lateinit var option8: Option
    private lateinit var option9: Option
    private lateinit var option10: Option
    private lateinit var option11: Option
    private lateinit var option12: Option
    private lateinit var option13: Option
    private lateinit var option14: Option
    private lateinit var option15: Option
    private lateinit var option16: Option
    private lateinit var option17: Option
    private lateinit var option18: Option
    private lateinit var option19: Option
    private lateinit var option20: Option
    private lateinit var option21: Option
    private lateinit var option22: Option
    private lateinit var option23: Option
    private lateinit var option24: Option
    private lateinit var option25: Option
    private lateinit var option26: Option
    private lateinit var option27: Option
    private lateinit var option28: Option
    private lateinit var option29: Option

    private final var SAMPLE_RATE = 16000.0f
    private final var PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    private lateinit var tvPartial : TextView
    private lateinit var tvResult : TextView

    private lateinit var tvOption1 : TextView
    private lateinit var tvOption2 : TextView
    private lateinit var tvOption3 : TextView
    private lateinit var tvOption4 : TextView
    private lateinit var tvOption5 : TextView
    private lateinit var listTvOptions : List<TextView>
    private lateinit var log : TextView

    private lateinit var model : Model
    private lateinit var speechService : SpeechService

    lateinit var randomizeList : List<Option>
    var gameInitialize = false
    var currentIndex = 0

    private var listOptions = listOf<String>(
        "numerico", "alfanumerico"
    )

    fun initOptions()
    {
        optionA = Option("A", listOf<String>("A"), )
        optionQ = Option("Q", listOf<String>("CU"), )
        optionK = Option("K", listOf<String>("CA"), )
        optionJ = Option("J", listOf<String>("JOTA"), )
        optionT = Option("T", listOf<String>("TE"), )
        option0 = Option("0",   listOf<String>("CERO"), )
        option1 = Option("1",   listOf<String>("UNO"), )
        option2 = Option("2",   listOf<String>("DOS", "NOS"), )
        option3 = Option("3",   listOf<String>("TRES"), )
        option4 = Option("4",   listOf<String>("CUATRO"), )
        option5 = Option("5",   listOf<String>("CINCO"), )
        option6 = Option("6",   listOf<String>("SEIS"), )
        option7 = Option("7",   listOf<String>("SIETE", "SIEMPRE"), )
        option8 = Option("8",   listOf<String>("OCHO"), )
        option9 = Option("9",   listOf<String>("NUEVE"), )
        option10 = Option("10", listOf<String>("DIEZ"), )
        option11 = Option("11", listOf<String>("ONCE"), )
        option12 = Option("12", listOf<String>("DOCE"), )
        option13 = Option("13", listOf<String>("TRECE"), )
        option14 = Option("14", listOf<String>("CATORCE"), )
        option15 = Option("15", listOf<String>("QUINCE"), )
        option16 = Option("16", listOf<String>("DIECISÉIS", "DIECISEIS"), )
        option17 = Option("17", listOf<String>("DIECISIETE"), )
        option18 = Option("18", listOf<String>("DIECIOCHO"), )
        option19 = Option("19", listOf<String>("DIECINUEVE"), )
        option20 = Option("20", listOf<String>("VEINTE"), )
        option21 = Option("21", listOf<String>("VEINTIUNO"), )
        option22 = Option("22", listOf<String>("VEINTIDÓS", "VEINTIDOS"), )
        option23 = Option("23", listOf<String>("VEINTITRÉS", "VEINTITRES"), )
        option24 = Option("24", listOf<String>("VEINTICUATRO"), )
        option25 = Option("25", listOf<String>("VEINTICINCO"), )
        option26 = Option("26", listOf<String>("VEINTISÉIS", "VEINTISEIS"), )
        option27 = Option("27", listOf<String>("VEINTISIETE"), )
        option28 = Option("28", listOf<String>("VEINTIOCHO"), )
        option29 = Option("29", listOf<String>("VEINTINUEVE"), )
    }

    fun initAlphaNumeric()
    {
        options = listOf<Option>(
            optionA, optionQ, optionK, optionJ, optionT,
            option0, option1, option2, option3, option4, option5, option6, option7, option8, option9,
        )
    }
    fun initNumeric()
    {
        options = listOf<Option>(
            option0, option1, option2, option3, option4, option5, option6, option7, option8, option9,
            option10, option11, option12, option13, option14, option15, option16, option17, option18, option19,
            option20, option21, option22, option23, option24, option25, option26, option27, option28, option29,
            )
    }
    fun initNumericOnlyTen()
    {
        options = listOf<Option>(
            option0, option1, option2, option3, option4, option5, option6, option7, option8, option9
            )
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvPartial = findViewById(R.id.partial)
        tvResult = findViewById(R.id.result)
        tvOption1 = findViewById(R.id.option1)
        tvOption2 = findViewById(R.id.option2)
        tvOption3 = findViewById(R.id.option3)
        tvOption4 = findViewById(R.id.option4)
        tvOption5 = findViewById(R.id.option5)
        listTvOptions = listOf<TextView>(tvOption1, tvOption2, tvOption3, tvOption4, tvOption5)
        log = findViewById(R.id.log)


//        rvOptionSelector = findViewById(R.id.rvOptionSelector)

        initOptions()


        LibVosk.setLogLevel(LogLevel.INFO);
        var check = ContextCompat.checkSelfPermission(
            applicationContext,
            android.Manifest.permission.RECORD_AUDIO)

        if (check != PackageManager.PERMISSION_GRANTED)
        {
            var permissionArray: Array<String> = arrayOf(android.Manifest.permission.RECORD_AUDIO);

            ActivityCompat.requestPermissions(
                this,
                permissionArray,
                PERMISSIONS_REQUEST_RECORD_AUDIO)
        }
        else
        {
            initModel();
        }
        //initRecyclereView()
        initAlphaNumeric()
    }

    private fun initModel()
    {
        StorageService.unpack(
            this,
//            "model-en-us",
            "model-es",

            "models",
            this::completeCallback,
            this::errorCallback)
    }
    private fun errorCallback(exception : IOException)
    {
        exception.printStackTrace()
    }
    private fun completeCallback(model : Model)
    {
        this.model = model;
        initMic()
    }
    private fun initMic()
    {
        try {
             var rec:Recognizer = Recognizer(model, SAMPLE_RATE,
                 "[\"cero\",\"uno\",\"dos\",\"tres\",\"cuatro\",\"cinco\",\"seis\",\"siete\",\"ocho\",\"nueve\", \"[diez]\", \"a\", \"ca\", \"cu\", \"jota\", \"te\"]"
             )
            speechService = SpeechService(rec, SAMPLE_RATE)
            speechService.startListening(this)
        }
        catch (e : Exception)
        {
            e.printStackTrace()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO)
        {
            initModel()
        }
        else
            Toast.makeText(applicationContext, "WITHOUT RECORD_AUDIO PERMISSION THE APP CAN'T WORK ", Toast.LENGTH_LONG).show()
    }
    override fun onFinalResult(hypothesis: String?) {
        Log.d("LOG", "onFinalResult:" + hypothesis)
    }

    override fun onError(exception: Exception?) {
        Log.e("LOG", "onError:" + exception.toString())
    }

    override fun onTimeout() {
        Log.d("LOG", "onTimeout")
    }


    fun onRadioButtonClick(view: View) {
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            if (checked) {
                when (view.getId()) {
                    R.id.radioNumeric ->
                        initNumeric()
                    R.id.radioAlphaNumeric ->
                        initAlphaNumeric()
                    R.id.radioNumericOnlyTen ->
                        initNumericOnlyTen()
                }
                startGame()
            }
        }

    }

    private fun startGame() {
        gameInitialize = true
        currentIndex = 0
        randomizeList = listOf<Option>(
            options[(0 .. options.count()-1).random()],
            options[(0 .. options.count()-1).random()],
            options[(0 .. options.count()-1).random()],
            options[(0 .. options.count()-1).random()],
            options[(0 .. options.count()-1).random()]
        )
        tvOption1.text = randomizeList[0].name
        tvOption2.text = randomizeList[1].name
        tvOption3.text = randomizeList[2].name
        tvOption4.text = randomizeList[3].name
        tvOption5.text = randomizeList[4].name
        tvOption1.setBackgroundColor(applicationContext.getColor(R.color.beforeTest))
        tvOption2.setBackgroundColor(applicationContext.getColor(R.color.beforeTest))
        tvOption3.setBackgroundColor(applicationContext.getColor(R.color.beforeTest))
        tvOption4.setBackgroundColor(applicationContext.getColor(R.color.beforeTest))
        tvOption5.setBackgroundColor(applicationContext.getColor(R.color.beforeTest))
    }
    override fun onPartialResult(hypothesis: String?) {
        if (!gameInitialize)
            return
        var jsonObject :JSONObject  = JSONObject(hypothesis)
        var text : String = jsonObject.getString("partial")
        tvPartial.text = "partial : " + text
    }

    override fun onResult(hypothesis: String?) {
        if (!gameInitialize)
            return

        var jsonObject :JSONObject  = JSONObject(hypothesis)
        var texts : String = jsonObject.getString("text")
        //Log.d("partial", "final: " + text)

        tvResult.text = "result : " + texts

        if(texts == "juego")
        {
            startGame()
            return
        }

        if (texts == "")
            return
        var words = texts.split(" ")
        words.forEach(){ text ->
            if (currentIndex == 5)
                return

            val currentTvOption = listTvOptions[currentIndex]
            var pass = false
            var option = randomizeList[currentIndex]
            option.words.forEach{
                if (text.lowercase() == it.lowercase())
                {
                    pass = true
                }
            }
            option.count ++
            if (pass){
                option.pass ++
                currentTvOption.setBackgroundColor(applicationContext.getColor(R.color.pass))
            }else{
                currentTvOption.setBackgroundColor(applicationContext.getColor(R.color.fail))
            }
            currentIndex ++
        }
        printInfo()
    }

    private fun printInfo() {
        var text : String = ""
        options.forEach(){
            if (it.count > 0 )
                text = "$text\n '${it.name}' % ${(it.pass / it.count)*100}"
            else
                text = "$text\n '${it.name}' % 0"
        }
        log.text = text
    }
    fun onButtonClick(view: View)  = startGame()
}
