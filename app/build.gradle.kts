import java.util.Properties
import java.io.FileInputStream
plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}


android {
    namespace = "com.sader_dev.vosk_voice_to_text_basic_implementation"
    compileSdk = 33

    defaultConfig {
        applicationId = "com.sader_dev.vosk_voice_to_text_basic_implementation"
        minSdk = 24
        targetSdk = 33
        versionCode = 3
        versionName = "0.0.3"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }


    signingConfigs {

        create("release") {
            keyAlias = System.getenv("keyAlias") as String
            keyPassword = System.getenv("keyPassword") as String
            storeFile = rootProject.file("/builds/sader_dev/vosk_voice_to_text_basic_implementation/.secure_files/release-keystore.jks")
            storePassword = System.getenv("storePassword").toString()
         }
    }

    buildTypes {
        release {
            signingConfig = signingConfigs.getByName("release")
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation("androidx.core:core-ktx:1.9.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.9.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.media3:media3-common:1.1.1")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")

    implementation("com.alphacephei:vosk-android:0.3.47")
    implementation(project(":models"))

//    implementation("java.util:java.util:18.0.1")



}
