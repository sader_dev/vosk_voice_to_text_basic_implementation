package com.sader_dev.vosk_voice_to_text_basic_implementation.adapter

import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.sader_dev.vosk_voice_to_text_basic_implementation.Option
import com.sader_dev.vosk_voice_to_text_basic_implementation.R

class listViewHolder(view: View) : ViewHolder(view) {

    val tvName = view.findViewById<TextView>(R.id.tvName)
    fun render(listItem : String)
    {
        tvName.text = listItem
        tvName.setOnClickListener {
            Toast.makeText(tvName.context, listItem, Toast.LENGTH_LONG ).show()
        }

    }
}